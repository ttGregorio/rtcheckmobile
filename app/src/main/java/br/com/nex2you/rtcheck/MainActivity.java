package br.com.nex2you.rtcheck;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.ExecutionException;

import br.com.nex2you.rtcheck.beans.Usuario;
import br.com.nex2you.rtcheck.services.UsuarioService;

public class MainActivity extends AppCompatActivity {

    private Button buttonEntrar;

    private EditText editTextLogin;

    private EditText editTextSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextLogin = (EditText)findViewById(R.id.editTextLogin);
        editTextSenha = (EditText)findViewById(R.id.editTextSenha);
        buttonEntrar = (Button) findViewById(R.id.buttonEntrar);
        buttonEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
                if(editTextLogin.getText().toString().trim().equals("")){
                    progressDialog.setMessage("Por favor, preencha o login do usuário.");
                    progressDialog.setTitle("ERRO");
                    progressDialog.show();
                }else if(editTextSenha.getText().toString().trim().equals("")){
                    progressDialog.setMessage("Por favor, preencha a senha do usuário.");
                    progressDialog.setTitle("ERRO");
                    progressDialog.show();
                }else{
                    String[]params = new String[2];
                    params[0] = editTextLogin.getText().toString();
                    params[1] = editTextSenha.getText().toString();

                    try {
                        Usuario usuario = new UsuarioService(progressDialog).execute(params).get();

                        if(usuario != null){
                            progressDialog.setMessage("Bem vindo ".concat(usuario.getNome()));
                            progressDialog.show();

                            Intent intent = new Intent(MainActivity.this, PrincipalActivity.class);
                            intent.putExtra("usuario", usuario);

                            startActivity(intent);

                            finish();
                        }else{
                            progressDialog.setMessage("Usuário e/ou Senha não localizados.");
                            progressDialog.setTitle("ERRO");
                            progressDialog.show();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
