/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Classe bean de dados do item de quest�o.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class ItemQuestao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id do item.
	 */
	private int id;

	/**
	 * Nome do item.
	 */
	private String nome;

	/**
	 * Unidade utilizada.
	 */
	private String unidade;

	/**
	 * Quest�o possuidora do item.
	 */
	private int questao;

	public boolean isNumerico() {
		return numerico;
	}

	public void setNumerico(boolean numerico) {
		this.numerico = numerico;
	}

	private boolean numerico;

	private List<ItemQuestao>itemQuestao;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo nome.
	 * 
	 * @return Valor de nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter do campo nome.
	 * 
	 * @param nome
	 *            Valor a ser carregado em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter do campo unidade.
	 * 
	 * @return Valor de unidade
	 */
	public String getUnidade() {
		return unidade;
	}

	/**
	 * Setter do campo unidade.
	 * 
	 * @param unidade
	 *            Valor a ser carregado em unidade
	 */
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	/**
	 * Getter do campo questao.
	 * 
	 * @return Valor de questao
	 */
	public int getQuestao() {
		return questao;
	}

	/**
	 * Setter do campo questao.
	 * 
	 * @param questao
	 *            Valor a ser carregado em questao
	 */
	public void setQuestao(int questao) {
		this.questao = questao;
	}

	public List<ItemQuestao> getItemQuestao() {
		return itemQuestao;
	}

	public void setItemQuestao(List<ItemQuestao> itemQuestao) {
		this.itemQuestao = itemQuestao;
	}
}
