/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;

/**
 * Classe bean contendo as respostas aos itens das quest�es.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class ItemQuestaoResposta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id da resposta.
	 */
	private int id;

	/**
	 * Id do item.
	 */
	private int item;

	/**
	 * Resposta fornecida.
	 */
	private String resposta;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo item.
	 * 
	 * @return Valor de item
	 */
	public int getItem() {
		return item;
	}

	/**
	 * Setter do campo item.
	 * 
	 * @param item
	 *            Valor a ser carregado em item
	 */
	public void setItem(int item) {
		this.item = item;
	}

	/**
	 * Getter do campo resposta.
	 * 
	 * @return Valor de resposta
	 */
	public String getResposta() {
		return resposta;
	}

	/**
	 * Setter do campo resposta.
	 * 
	 * @param resposta
	 *            Valor a ser carregado em resposta
	 */
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

}
