package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thiago on 09/04/2016.
 */
public class MapaTopicos implements Serializable {

    public MapaTopicos(){
        setMapaRespostas(new HashMap<Integer, Map<Integer, OrdemServicoResposta>>());
    }
    private Map<Integer,Map<Integer, OrdemServicoResposta>> mapaRespostas;

    public Map<Integer, Map<Integer, OrdemServicoResposta>> getMapaRespostas() {
        return mapaRespostas;
    }

    public void setMapaRespostas(Map<Integer, Map<Integer, OrdemServicoResposta>> mapaRespostas) {
        this.mapaRespostas = mapaRespostas;
    }
}
