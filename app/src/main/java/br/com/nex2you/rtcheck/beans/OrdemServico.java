/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Classe bean contendo os dados relativos a ordens de servi�o.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class OrdemServico implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrdemServico() {
		setQuestionario(new Questionario());
	}

	private List<OrdemServico>ordemServico;

	/**
	 * Id da ordem de servi�o.
	 */
	private int id;

	/**
	 * Placa do ve�culo.
	 */
	private String placa;

	/**
	 * Question�rio desta ordem.
	 */
	private Questionario questionario;

	/**
	 * Data de elabora��o desta ordem.
	 */
	private Date dataElaboracao;

	/**
	 * Data de aprova��o desta ordem.
	 */
	private Date dataAprovacao;

	/**
	 * Usu�rio aprovador desta ordem.
	 */
	private int usuarioAprovador;

	/**
	 * Usu�rio elaborador desta ordem.
	 */
	private int usuarioElaborador;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo placa.
	 * 
	 * @return Valor de placa
	 */
	public String getPlaca() {
		return placa;
	}

	/**
	 * Setter do campo placa.
	 * 
	 * @param placa
	 *            Valor a ser carregado em placa
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}

	/**
	 * Getter do campo questionario.
	 * 
	 * @return Valor de questionario
	 */
	public Questionario getQuestionario() {
		return questionario;
	}

	/**
	 * Setter do campo questionario.
	 * 
	 * @param questionario
	 *            Valor a ser carregado em questionario
	 */
	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}

	/**
	 * Getter do campo dataElaboracao.
	 * 
	 * @return Valor de dataElaboracao
	 */
	public Date getDataElaboracao() {
		return dataElaboracao;
	}

	/**
	 * Setter do campo dataElaboracao.
	 * 
	 * @param dataElaboracao
	 *            Valor a ser carregado em dataElaboracao
	 */
	public void setDataElaboracao(Date dataElaboracao) {
		this.dataElaboracao = dataElaboracao;
	}

	/**
	 * Getter do campo dataAprovacao.
	 * 
	 * @return Valor de dataAprovacao
	 */
	public Date getDataAprovacao() {
		return dataAprovacao;
	}

	/**
	 * Setter do campo dataAprovacao.
	 * 
	 * @param dataAprovacao
	 *            Valor a ser carregado em dataAprovacao
	 */
	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	/**
	 * Getter do campo usuarioAprovador.
	 * 
	 * @return Valor de usuarioAprovador
	 */
	public int getUsuarioAprovador() {
		return usuarioAprovador;
	}

	/**
	 * Setter do campo usuarioAprovador.
	 * 
	 * @param usuarioAprovador
	 *            Valor a ser carregado em usuarioAprovador
	 */
	public void setUsuarioAprovador(int usuarioAprovador) {
		this.usuarioAprovador = usuarioAprovador;
	}

	/**
	 * Getter do campo usuarioElaborador.
	 * 
	 * @return Valor de usuarioElaborador
	 */
	public int getUsuarioElaborador() {
		return usuarioElaborador;
	}

	/**
	 * Setter do campo usuarioElaborador.
	 * 
	 * @param usuarioElaborador
	 *            Valor a ser carregado em usuarioElaborador
	 */
	public void setUsuarioElaborador(int usuarioElaborador) {
		this.usuarioElaborador = usuarioElaborador;
	}

	public List<OrdemServico> getOrdemServico() {
		return ordemServico;
	}

	public void setOrdemServico(List<OrdemServico> ordemServico) {
		this.ordemServico = ordemServico;
	}
}
