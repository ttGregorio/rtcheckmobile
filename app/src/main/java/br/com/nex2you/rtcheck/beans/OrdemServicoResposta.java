/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;

/**
 * Classe bean contendo os dados relativos a ordens de servi�o.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class OrdemServicoResposta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrdemServicoResposta() {
		setOrdemServico(new OrdemServico());
		setQuestao(new Questao());
		setTopico(new Topico());
	}

	/**
	 * Id da ordem de servi�o.
	 */
	private int id;

	/**
	 * Resposta data.
	 */
	private String resposta;

	/**
	 * Ordem de Servi�o possuidora da resposta.
	 */
	private OrdemServico ordemServico;

	/**
	 * Quest�o respondida.
	 */
	private Questao questao;

	/**
	 * T�pico possuidor da questao.
	 */
	private Topico topico;

	/**
	 * Valor a direita informado pelo usuário.
	 */
	private double valorDireita;

	/**
	 * Valor a esquerda informado pelo usuário.
	 */
	private double valorEsquerda;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo resposta.
	 * 
	 * @return Valor de resposta
	 */
	public String getResposta() {
		return resposta;
	}

	/**
	 * Setter do campo resposta.
	 * 
	 * @param resposta
	 *            Valor a ser carregado em resposta
	 */
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	/**
	 * Getter do campo ordemServico.
	 * 
	 * @return Valor de ordemServico
	 */
	public OrdemServico getOrdemServico() {
		return ordemServico;
	}

	/**
	 * Setter do campo ordemServico.
	 * 
	 * @param ordemServico
	 *            Valor a ser carregado em ordemServico
	 */
	public void setOrdemServico(OrdemServico ordemServico) {
		this.ordemServico = ordemServico;
	}

	/**
	 * Getter do campo questao.
	 * 
	 * @return Valor de questao
	 */
	public Questao getQuestao() {
		return questao;
	}

	/**
	 * Setter do campo questao.
	 * 
	 * @param questao
	 *            Valor a ser carregado em questao
	 */
	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	/**
	 * Getter do campo topico.
	 * 
	 * @return Valor de topico
	 */
	public Topico getTopico() {
		return topico;
	}

	/**
	 * Setter do campo topico.
	 * 
	 * @param topico
	 *            Valor a ser carregado em topico
	 */
	public void setTopico(Topico topico) {
		this.topico = topico;
	}


	/**
	 * Getter do campo valorDireita.
	 *
	 * @return Valor de valorDireita
	 */
	public double getValorDireita() {
		return valorDireita;
	}

	/**
	 * Setter do campo valorDireita.
	 *
	 * @param valorDireita
	 *            Valor a ser carregado em valorDireita
	 */
	public void setValorDireita(double valorDireita) {
		this.valorDireita = valorDireita;
	}

	/**
	 * Getter do campo valorEsquerda.
	 *
	 * @return Valor de valorEsquerda
	 */
	public double getValorEsquerda() {
		return valorEsquerda;
	}

	/**
	 * Setter do campo valorEsquerda.
	 *
	 * @param valorEsquerda
	 *            Valor a ser carregado em valorEsquerda
	 */
	public void setValorEsquerda(double valorEsquerda) {
		this.valorEsquerda = valorEsquerda;
	}


}
