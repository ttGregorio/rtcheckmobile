/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;

/**
 * Classe bean de dados referente aos dados do perfil do usu�rio.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class Perfil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id do perfil.
	 */
	private int id;

	/**
	 * Nome do perfil.
	 */
	private String nome;

	/**
	 * Flag indicando se o perfil est� ou n�o ativo.
	 */
	private boolean ativo;

	/**
	 * Flag indicando se o perfil � ou n�o pass�vel da aprova��o de ordens de
	 * servi�o.
	 */
	private boolean aprovador;

	/**
	 * Flag indicando se o perfil � ou n�o pass�vel da abora��o de ordens de
	 * servi�o.
	 */
	private boolean elaborador;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo nome.
	 * 
	 * @return Valor de nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter do campo nome.
	 * 
	 * @param nome
	 *            Valor a ser carregado em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter do campo ativo.
	 * 
	 * @return Valor de ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter do campo ativo.
	 * 
	 * @param ativo
	 *            Valor a ser carregado em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	/**
	 * Getter do campo aprovador.
	 * 
	 * @return Valor de aprovador
	 */
	public boolean isAprovador() {
		return aprovador;
	}

	/**
	 * Setter do campo aprovador.
	 * 
	 * @param aprovador
	 *            Valor a ser carregado em aprovador
	 */
	public void setAprovador(boolean aprovador) {
		this.aprovador = aprovador;
	}

	/**
	 * Getter do campo elaborador.
	 * 
	 * @return Valor de elaborador
	 */
	public boolean isElaborador() {
		return elaborador;
	}

	/**
	 * Setter do campo elaborador.
	 * 
	 * @param elaborador
	 *            Valor a ser carregado em elaborador
	 */
	public void setElaborador(boolean elaborador) {
		this.elaborador = elaborador;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Perfil)) {
			return false;
		}
		final Perfil other = (Perfil) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}

}
