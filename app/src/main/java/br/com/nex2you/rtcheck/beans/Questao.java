package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Classe bean de dados referente aos dados das quest�es do question�rio.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class Questao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Questao> questao;
	/**
	 * Id da quest�o.
	 */
	private int id;

	/**
	 * Quest�o.
	 */
	private String nome;

	/**
	 * Flag indicando se esta quest�o est� ou n�o ativa.
	 */
	private boolean ativo;

	/**
	 * Flag indicando se este campo apresentará campos para inclusão de valores
	 * de direita e esquerda.
	 */
	private boolean campoDireitaEsquerda;

	/**
	 * Descrição do que se trata esta questão.
	 */
	private String descricao;

	private double qtdCamposValor;

	private String unidade;

	private List<ItemQuestao>listaItens;

	private Map<Integer, ItemQuestaoResposta>mapaRespostas;

	private String imagem;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo nome.
	 * 
	 * @return Valor de nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter do campo nome.
	 * 
	 * @param nome
	 *            Valor a ser carregado em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter do campo ativo.
	 * 
	 * @return Valor de ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter do campo ativo.
	 * 
	 * @param ativo
	 *            Valor a ser carregado em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isCampoDireitaEsquerda() {
		return campoDireitaEsquerda;
	}

	public void setCampoDireitaEsquerda(boolean campoDireitaEsquerda) {
		this.campoDireitaEsquerda = campoDireitaEsquerda;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Questao> getQuestao() {
		return questao;
	}

	public void setQuestao(List<Questao> questao) {
		this.questao = questao;
	}

	public double getQtdCamposValor() {
		return qtdCamposValor;
	}

	public void setQtdCamposValor(double qtdCamposValor) {
		this.qtdCamposValor = qtdCamposValor;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public List<ItemQuestao> getListaItens() {
		return listaItens;
	}

	public void setListaItens(List<ItemQuestao> listaItens) {
		this.listaItens = listaItens;
	}

	public Map<Integer, ItemQuestaoResposta> getMapaRespostas() {
		return mapaRespostas;
	}

	public void setMapaRespostas(Map<Integer, ItemQuestaoResposta> mapaRespostas) {
		this.mapaRespostas = mapaRespostas;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Perfil)) {
			return false;
		}
		final Questao other = (Questao) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}
}
