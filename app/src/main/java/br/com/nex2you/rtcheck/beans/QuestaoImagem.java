/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Classe bean das imagens
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class QuestaoImagem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public QuestaoImagem() {
		setChave(Math.random() * Math.random() * 10000 + Math.random() * Math.random() * 100000);
		setQuestao(new Questao());
	}

	private List<QuestaoImagem> questaoImagem;

	/**
	 * Id da imagem.
	 */
	private int id;

	/**
	 * Quest�o possuidora da imagem.
	 */
	private Questao questao;

	/**
	 * Imagem.
	 */
	private String imagem;

	private String descricao;

	private int coluna;

	private int linha;

	private double chave;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo questao.
	 * 
	 * @return Valor de questao
	 */
	public Questao getQuestao() {
		return questao;
	}

	/**
	 * Setter do campo questao.
	 * 
	 * @param questao
	 *            Valor a ser carregado em questao
	 */
	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	/**
	 * Getter do campo imagem.
	 * 
	 * @return Valor de imagem
	 */
	public String getImagem() {
		return imagem;
	}

	/**
	 * Setter do campo imagem.
	 * 
	 * @param imagem
	 *            Valor a ser carregado em imagem
	 */
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	/**
	 * Getter do campo descricao.
	 * 
	 * @return Valor de descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Setter do campo descricao.
	 * 
	 * @param descricao
	 *            Valor a ser carregado em descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Getter do campo coluna.
	 * 
	 * @return Valor de coluna
	 */
	public int getColuna() {
		return coluna;
	}

	/**
	 * Setter do campo coluna.
	 * 
	 * @param coluna
	 *            Valor a ser carregado em coluna
	 */
	public void setColuna(int coluna) {
		this.coluna = coluna;
	}

	/**
	 * Getter do campo linha.
	 * 
	 * @return Valor de linha
	 */
	public int getLinha() {
		return linha;
	}

	/**
	 * Setter do campo linha.
	 * 
	 * @param linha
	 *            Valor a ser carregado em linha
	 */
	public void setLinha(int linha) {
		this.linha = linha;
	}

	/**
	 * @return the chave
	 */
	public double getChave() {
		return chave;
	}

	/**
	 * @param chave
	 *            the chave to set
	 */
	public void setChave(double chave) {
		this.chave = chave;
	}

	public List<QuestaoImagem> getQuestaoImagem() {
		return questaoImagem;
	}

	public void setQuestaoImagem(List<QuestaoImagem> questaoImagem) {
		this.questaoImagem = questaoImagem;
	}
}
