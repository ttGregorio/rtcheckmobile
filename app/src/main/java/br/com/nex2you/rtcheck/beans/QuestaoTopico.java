/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;

/**
 * Classe bean de dados referente aos dados das quest�es de um t�pico.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class QuestaoTopico implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public QuestaoTopico() {
		setQuestao(new Questao());
		setTopico(new Topico());
	}

	/**
	 * Id da conex�o.
	 */
	private int id;

	/**
	 * Quest�o pertecente ao t�pico.
	 */
	private Questao questao;

	/**
	 * T�pico possuidor da quest�o.
	 */
	private Topico topico;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo questao.
	 * 
	 * @return Valor de questao
	 */
	public Questao getQuestao() {
		return questao;
	}

	/**
	 * Setter do campo questao.
	 * 
	 * @param questao
	 *            Valor a ser carregado em questao
	 */
	public void setQuestao(Questao questao) {
		this.questao = questao;
	}

	/**
	 * Getter do campo topico.
	 * 
	 * @return Valor de topico
	 */
	public Topico getTopico() {
		return topico;
	}

	/**
	 * Setter do campo topico.
	 * 
	 * @param topico
	 *            Valor a ser carregado em topico
	 */
	public void setTopico(Topico topico) {
		this.topico = topico;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Usuario)) {
			return false;
		}
		final QuestaoTopico other = (QuestaoTopico) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}

}
