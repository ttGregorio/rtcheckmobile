package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Classe bean de dados referente aos dados dos question�rios.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class Questionario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Questionario>questionario;
	/**
	 * Id do questionario.
	 */
	private int id;

	/**
	 * Nome do question�rio.
	 */
	private String nome;

	/**
	 * Flag indicando se este question�rio est� ou n�o ativo.
	 */
	private boolean ativo;

	/**
	 * Lista de t�picos pertencentes a este question�rio.
	 */
	private List<Topico> listaTopicos;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo nome.
	 * 
	 * @return Valor de nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter do campo nome.
	 * 
	 * @param nome
	 *            Valor a ser carregado em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter do campo ativo.
	 * 
	 * @return Valor de ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter do campo ativo.
	 * 
	 * @param ativo
	 *            Valor a ser carregado em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Perfil)) {
			return false;
		}
		final Questionario other = (Questionario) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}

	/**
	 * Getter do campo listaTopicos.
	 * 
	 * @return Valor de listaTopicos
	 */
	public List<Topico> getListaTopicos() {
		return listaTopicos;
	}

	/**
	 * Setter do campo listaTopicos.
	 * 
	 * @param listaTopicos
	 *            Valor a ser carregado em listaTopicos
	 */
	public void setListaTopicos(List<Topico> listaTopicos) {
		this.listaTopicos = listaTopicos;
	}

	public List<Questionario> getQuestionario() {
		return questionario;
	}

	public void setQuestionario(List<Questionario> questionario) {
		this.questionario = questionario;
	}
}
