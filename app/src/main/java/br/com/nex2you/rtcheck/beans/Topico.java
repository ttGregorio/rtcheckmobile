package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Classe bean de dados referente aos dados dos t�picos do question�rio.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class Topico implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Topico>topico;
	/**
	 * Id da t�pico.
	 */
	private int id;

	/**
	 * Nome do T�pico.
	 */
	private String nome;

	/**
	 * Flag indicando se este t�pico est� ou n�o ativo.
	 */
	private boolean ativo;

	/**
	 * Lista de quest�es pertencentes a este t�pico.
	 */
	private List<Questao> listaQuestoes;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo nome.
	 * 
	 * @return Valor de nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter do campo nome.
	 * 
	 * @param nome
	 *            Valor a ser carregado em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter do campo ativo.
	 * 
	 * @return Valor de ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter do campo ativo.
	 * 
	 * @param ativo
	 *            Valor a ser carregado em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Perfil)) {
			return false;
		}
		final Topico other = (Topico) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}

	/**
	 * Getter do campo listaQuestoes.
	 * 
	 * @return Valor de listaQuestoes
	 */
	public List<Questao> getListaQuestoes() {
		return listaQuestoes;
	}

	/**
	 * Setter do campo listaQuestoes.
	 * 
	 * @param listaQuestoes
	 *            Valor a ser carregado em listaQuestoes
	 */
	public void setListaQuestoes(List<Questao> listaQuestoes) {
		this.listaQuestoes = listaQuestoes;
	}

	public List<Topico> getTopico() {
		return topico;
	}

	public void setTopico(List<Topico> topico) {
		this.topico = topico;
	}
}
