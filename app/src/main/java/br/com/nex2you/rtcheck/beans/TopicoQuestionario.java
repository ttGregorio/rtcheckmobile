/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;

/**
 * Classe bean de dados referente aos dados dos t�picos de um question�rio.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class TopicoQuestionario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TopicoQuestionario() {
		setQuestionario(new Questionario());
		setTopico(new Topico());
	}

	/**
	 * Id da conex�o.
	 */
	private int id;

	/**
	 * Questionario possuidor do t�pico.
	 */
	private Questionario questionario;

	/**
	 * T�pico pertencente ao question�rio.
	 */
	private Topico topico;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo questionario.
	 * 
	 * @return Valor de questionario
	 */
	public Questionario getQuestionario() {
		return questionario;
	}

	/**
	 * Setter do campo questionario.
	 * 
	 * @param questionario
	 *            Valor a ser carregado em questionario
	 */
	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}

	/**
	 * Getter do campo topico.
	 * 
	 * @return Valor de topico
	 */
	public Topico getTopico() {
		return topico;
	}

	/**
	 * Setter do campo topico.
	 * 
	 * @param topico
	 *            Valor a ser carregado em topico
	 */
	public void setTopico(Topico topico) {
		this.topico = topico;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Usuario)) {
			return false;
		}
		final TopicoQuestionario other = (TopicoQuestionario) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}

}
