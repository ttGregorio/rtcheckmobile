/**
 * 
 */
package br.com.nex2you.rtcheck.beans;

import java.io.Serializable;

/**
 * Classe bean de dados referente aos dados do usu�rio.
 * 
 * @author Thiago Tavares Gregorio
 *
 */
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Usuario() {
		setPerfil(new Perfil());
	}

	/**
	 * Id do Usu�rio.
	 */
	private int id;

	/**
	 * Nome do Usu�rio.
	 */
	private String nome;

	/**
	 * Login do Usu�rio.
	 */
	private String login;

	/**
	 * Senha do Usu�rio.
	 */
	private String senha;

	/**
	 * Flag indicando se o Usu�rio est� ou n�o ativo.
	 */
	private boolean ativo;

	/**
	 * Perfil do Usu�rio.
	 */
	private Perfil perfil;

	/**
	 * Getter do campo id.
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter do campo id.
	 * 
	 * @param id
	 *            Valor a ser carregado em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter do campo nome.
	 * 
	 * @return Valor de nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter do campo nome.
	 * 
	 * @param nome
	 *            Valor a ser carregado em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter do campo login.
	 * 
	 * @return Valor de login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Setter do campo login.
	 * 
	 * @param login
	 *            Valor a ser carregado em login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Getter do campo senha.
	 * 
	 * @return Valor de senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Setter do campo senha.
	 * 
	 * @param senha
	 *            Valor a ser carregado em senha
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Getter do campo ativo.
	 * 
	 * @return Valor de ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter do campo ativo.
	 * 
	 * @param ativo
	 *            Valor a ser carregado em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	/**
	 * Getter do campo perfil.
	 * 
	 * @return Valor de perfil
	 */
	public Perfil getPerfil() {
		return perfil;
	}

	/**
	 * Setter do campo perfil.
	 * 
	 * @param perfil
	 *            Valor a ser carregado em perfil
	 */
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Usuario)) {
			return false;
		}
		final Usuario other = (Usuario) obj;
		return getId() == other.getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}

}
