package br.com.nex2you.rtcheck.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import br.com.nex2you.rtcheck.beans.Questao;
import br.com.nex2you.rtcheck.services.utils.Nex2YouService;

/**
 * Classe de gestão do serviço de consulta de questões.
 * Created by Thiago on 04/04/2016.
 */
public class BuscarQuestaoService extends AsyncTask<String, Void, List<Questao>> implements Nex2YouService{

    /**
     * URL de acesso a consulta de questões.
     */
    private String URL_QUESTOES_TOPICOS = "";

    /**
     * Progress Dialog.
     */
    private ProgressDialog progressDialog;

    public BuscarQuestaoService(ProgressDialog progressDialog){
        URL_QUESTOES_TOPICOS = URL_CONEXAO.concat("QuestaoService/listaQuestoes/");
        this.progressDialog = progressDialog;
        this.progressDialog.setMessage("Consultando questões...");
        this.progressDialog.show();
    }

    @Override
    protected List<Questao> doInBackground(String... params) {
        String parametros = "";
        for (int i = 0; i < params.length; i++) {
            parametros = parametros.concat(URLEncoder.encode(params[i])).concat(",");
        }

        return buscarTopicos(parametros.substring(0, parametros.length() - 1));
    }

    /**
     * Método de busca de questões.
     * @param parametros Parametros concatenados para envio.
     * @return Lista de ordens de serviço localizadas.
     */
    private List<Questao> buscarTopicos(String parametros) {
        List<Questao> questoes = null;
        URL urlRote;
        try {
            urlRote = new URL(URL_QUESTOES_TOPICOS.concat(parametros));
            HttpURLConnection conexao = (HttpURLConnection) urlRote.openConnection();
            conexao.connect();
            InputStream is = conexao.getInputStream();
            questoes = parserQuestao(is);
        } catch (Exception e) {
        }

        this.progressDialog.dismiss();
        return questoes;
    }

    /**
     * Parser do InputStream recebido do servidor para uma lista de objetos do sistema.
     * @param is InputStream Recebido.
     * @return Lista de objetos localizados.
     */
    private List<Questao> parserQuestao(InputStream is) {
        Questao questoes = null;
        BufferedReader streamReader;
        try {
            streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            JsonElement elem = new JsonParser().parse(streamReader);
            Gson gson = new GsonBuilder().create();
            questoes = gson.fromJson(elem, Questao.class);
        } catch (UnsupportedEncodingException e) {
        }
        if(questoes.getQuestao() != null && questoes.getQuestao().size() > 0){
            questoes.getQuestao().remove(0);
            questoes.getQuestao().remove(0);

            return questoes.getQuestao();
        }else {
            return new ArrayList<Questao>();
        }
    }
}
