package br.com.nex2you.rtcheck.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import br.com.nex2you.rtcheck.beans.Questionario;
import br.com.nex2you.rtcheck.services.utils.Nex2YouService;

/**
 * Classe de gestão do serviço de consulta de questionários.
 * Created by Thiago on 04/04/2016.
 */
public class BuscarQuestionarioService extends AsyncTask<String, Void, List<Questionario>> implements Nex2YouService{

    /**
     * URL de acesso a consulta de questionários.
     */
    private String URL_BUSCA_ORDENS_SERVICO = "";

    /**
     * Progress Dialog.
     */
    private ProgressDialog progressDialog;

    public BuscarQuestionarioService(ProgressDialog progressDialog){
        URL_BUSCA_ORDENS_SERVICO = URL_CONEXAO.concat("QuestionarioService/buscarQuestionarios/");
        this.progressDialog = progressDialog;
        this.progressDialog.setMessage("Consultando questionários...");
        this.progressDialog.show();
    }

    @Override
    protected List<Questionario> doInBackground(String... params) {
        return buscarQuestionarios("");
    }

    /**
     * Método de busca de questionários.
     * @param parametros Parametros concatenados para envio.
     * @return Lista de questionários
     * localizadas.
     */
    private List<Questionario> buscarQuestionarios(String parametros) {
        List<Questionario> questionarios = null;
        URL urlRote;
        try {
            urlRote = new URL(URL_BUSCA_ORDENS_SERVICO.concat(parametros));
            HttpURLConnection conexao = (HttpURLConnection) urlRote.openConnection();
            conexao.connect();
            InputStream is = conexao.getInputStream();
            questionarios = parserQuestionarios(is);
        } catch (Exception e) {
        }

        this.progressDialog.dismiss();
        return questionarios;
    }

    /**
     * Parser do InputStream recebido do servidor para uma lista de objetos do sistema.
     * @param is InputStream Recebido.
     * @return Lista de objetos localizados.
     */
    private List<Questionario> parserQuestionarios(InputStream is) {
        Questionario questionarios = null;
        BufferedReader streamReader;
        try {
            streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            JsonElement elem = new JsonParser().parse(streamReader);
            Gson gson = new GsonBuilder().create();
            questionarios = gson.fromJson(elem, Questionario.class);
        } catch (UnsupportedEncodingException e) {
        }
        if(questionarios.getQuestionario() != null && questionarios.getQuestionario().size() > 0){
            questionarios.getQuestionario().remove(0);
            questionarios.getQuestionario().remove(0);

            return questionarios.getQuestionario();
        }else {
            return new ArrayList<Questionario>();
        }
    }
}
