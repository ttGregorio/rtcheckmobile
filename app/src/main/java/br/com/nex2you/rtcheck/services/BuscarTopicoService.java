package br.com.nex2you.rtcheck.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import br.com.nex2you.rtcheck.beans.Topico;
import br.com.nex2you.rtcheck.services.utils.Nex2YouService;

/**
 * Classe de gestão do serviço de consulta de tópicos.
 * Created by Thiago on 04/04/2016.
 */
public class BuscarTopicoService extends AsyncTask<String, Void, List<Topico>> implements Nex2YouService{

    /**
     * URL de acesso a consulta de tópicos.
     */
    private String URL_BUSCA_TOPICOS = "";

    /**
     * Progress Dialog.
     */
    private ProgressDialog progressDialog;

    public BuscarTopicoService(ProgressDialog progressDialog){
        URL_BUSCA_TOPICOS = URL_CONEXAO.concat("TopicoService/buscarTopicos/");
        this.progressDialog = progressDialog;
        this.progressDialog.setMessage("Consultando tópicos...");
        this.progressDialog.show();
    }

    @Override
    protected List<Topico> doInBackground(String... params) {
        String parametros = "";
        for (int i = 0; i < params.length; i++) {
            parametros = parametros.concat(URLEncoder.encode(params[i])).concat(",");
        }

        return buscarTopicos(parametros.substring(0, parametros.length() - 1));
    }

    /**
     * Método de busca de tópicos.
     * @param parametros Parametros concatenados para envio.
     * @return Lista de ordens de serviço localizadas.
     */
    private List<Topico> buscarTopicos(String parametros) {
        List<Topico> topicos = null;
        URL urlRote;
        try {
            urlRote = new URL(URL_BUSCA_TOPICOS.concat(parametros));
            HttpURLConnection conexao = (HttpURLConnection) urlRote.openConnection();
            conexao.connect();
            InputStream is = conexao.getInputStream();
            topicos = parserTopico(is);
        } catch (Exception e) {
        }

        this.progressDialog.dismiss();
        return topicos;
    }

    /**
     * Parser do InputStream recebido do servidor para uma lista de objetos do sistema.
     * @param is InputStream Recebido.
     * @return Lista de objetos localizados.
     */
    private List<Topico> parserTopico(InputStream is) {
        Topico topico = null;
        BufferedReader streamReader;
        try {
            streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            JsonElement elem = new JsonParser().parse(streamReader);
            Gson gson = new GsonBuilder().create();
            topico = gson.fromJson(elem, Topico.class);
        } catch (UnsupportedEncodingException e) {
        }
        if(topico.getTopico() != null && topico.getTopico().size() > 0){
            topico.getTopico().remove(0);
            topico.getTopico().remove(0);

            return topico.getTopico();
        }else {
            return new ArrayList<Topico>();
        }
    }
}
