package br.com.nex2you.rtcheck.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import br.com.nex2you.rtcheck.beans.ItemQuestaoResposta;
import br.com.nex2you.rtcheck.services.utils.Nex2YouService;

/**
 * Classe de gestão do serviço de inserção de respostas a ordens de serviço.
 * Created by Thiago on 04/04/2016.
 */
public class InserirItemQuestaoRespostaService extends AsyncTask<String, Void, ItemQuestaoResposta> implements Nex2YouService{

    /**
     * URL de acesso a inserção de respostas ordens de serviço.
     */
    private String URL_INSERIR_RESPOSTAS_ORDENS_SERVICO = "";

    /**
     * Progress Dialog.
     */
    private ProgressDialog progressDialog;

    public InserirItemQuestaoRespostaService(ProgressDialog progressDialog){
        URL_INSERIR_RESPOSTAS_ORDENS_SERVICO = URL_CONEXAO.concat("ItemOrdemServicoService/inserirResposta/");
        this.progressDialog = progressDialog;
        this.progressDialog.setMessage("Inserindo resposta...");
        this.progressDialog.show();
    }

    @Override
    protected ItemQuestaoResposta doInBackground(String... params) {
        String parametros = "";
        for (int i = 0; i < params.length; i++) {
            parametros = parametros.concat(URLEncoder.encode(params[i])).concat(",");
        }

        return inserirRespostaOrdensServico(parametros.substring(0, parametros.length() - 1));
    }

    /**
     * Método de inserção de respostas para a ordens de serviço.
     * @param parametros Parametros concatenados para envio.
     * @return Ordem de Serviço inserida.
     */
    private ItemQuestaoResposta inserirRespostaOrdensServico(String parametros) {
        ItemQuestaoResposta ordens = null;
        URL urlRote;
        try {
            urlRote = new URL(URL_INSERIR_RESPOSTAS_ORDENS_SERVICO.concat(parametros));
            HttpURLConnection conexao = (HttpURLConnection) urlRote.openConnection();
            conexao.connect();
            InputStream is = conexao.getInputStream();
            ordens = parserOrdensServico(is);
        } catch (Exception e) {
        }

        this.progressDialog.dismiss();
        return ordens;
    }

    /**
     * Parser do InputStream recebido do servidor para uma lista de objetos do sistema.
     * @param is InputStream Recebido.
     * @return Lista de objetos localizados.
     */
    private ItemQuestaoResposta parserOrdensServico(InputStream is) {
        ItemQuestaoResposta ordensServico = null;
        BufferedReader streamReader;
        try {
            streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            JsonElement elem = new JsonParser().parse(streamReader);
            Gson gson = new GsonBuilder().create();
            ordensServico = gson.fromJson(elem, ItemQuestaoResposta.class);
        } catch (UnsupportedEncodingException e) {
        }
        if(ordensServico != null){
            return ordensServico;
        }else {
            return new ItemQuestaoResposta();
        }
    }
}
