package br.com.nex2you.rtcheck.services;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Thiago on 15/04/2016.
 */
public class RecuperarImagemService extends AsyncTask<String,Void,Bitmap> {
    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap bmp =null;
        try {
           URL url = new URL(params[0]);
            InputStream in = url.openStream();
           bmp = BitmapFactory.decodeStream(in);
    } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmp;
    }
}
