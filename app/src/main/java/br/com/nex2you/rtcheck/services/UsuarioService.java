package br.com.nex2you.rtcheck.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import br.com.nex2you.rtcheck.beans.Usuario;
import br.com.nex2you.rtcheck.services.utils.Nex2YouService;

/**
 * Classe de gestão do serviço relativo ao login do usuário.
 * Created by Thiago on 04/04/2016.
 */
public class UsuarioService extends AsyncTask<String, Void, Usuario> implements Nex2YouService{

    /**
     * URL de conexão com o serviço de login de usuários.
     */
    private String URL_LOGIN_USUARIOS = "";

    /**
     * Progress Dialog
     */
    private ProgressDialog progressDialog;

    public UsuarioService(ProgressDialog progressDialog){
        URL_LOGIN_USUARIOS = URL_CONEXAO.concat("UsuarioService/logarUsuario/");
        this.progressDialog = progressDialog;
        this.progressDialog.show();
    }

    @Override
    protected Usuario doInBackground(String... params) {
        String parametros = "";
        for (int i = 0; i < params.length; i++) {
            parametros = parametros.concat(URLEncoder.encode(params[i])).concat(",");
        }
        return logarUsuario(parametros.substring(0, parametros.length() - 1));
    }

    /**
     * Método de chamada do login de usuário, validando os dados de usuário e senha no servidor.
     * @param parametros login informado e senha separados por vírgulo.
     * @return Usuário localizado.
     */
    private Usuario logarUsuario(String parametros) {
        Usuario usuario = null;
        URL urlRote;
        try {
            urlRote = new URL(URL_LOGIN_USUARIOS.concat(parametros));
            HttpURLConnection conexao = (HttpURLConnection) urlRote.openConnection();
            conexao.connect();
            InputStream is = conexao.getInputStream();
            usuario = parserUser(is);
        } catch (Exception e) {
        }

        return usuario;
    }

    /**
     * Parser do inputstream recebido do servidor para objeto do sistema.
     * @param is InputStream recebido.
     * @return Objeto convertido.
     */
    private Usuario parserUser(InputStream is) {
        Usuario usuario = null;
        BufferedReader streamReader;
        try {
            streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            JsonElement elem = new JsonParser().parse(streamReader);
            Gson gson = new GsonBuilder().create();
            usuario = gson.fromJson(elem, Usuario.class);
        } catch (UnsupportedEncodingException e) {
        }
        return usuario;
    }
}
